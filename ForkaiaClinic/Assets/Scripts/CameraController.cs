﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    bool freeCam;
    GameObject player;
    Vector3 offset;
    Quaternion defaultRotation;
    GameManagerController GameManager;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        offset = transform.position - player.transform.position;
        freeCam = true;
        GameManager = GameManagerController.getInstance();
        GameManager.SetCamera(GetComponent<CameraController>());
    }

    // Update is called once per frame
    void Update()
    {
        if (freeCam)
        {
            //without smoothing
            //transform.position = player.transform.position + offset;

            //with smoothing
            transform.position = Vector3.Lerp(transform.position, player.transform.position + offset, 5f * Time.deltaTime);
        }
    }


    /*These should be Coroutines so everything looks smooth*/

    //function to swoop into dialogPos and look at dialog agent (temporary, not smoothed)
    public void SwoopIn(Vector3 position, Vector3 lookAtPos)
    {
        if (freeCam)
            freeCam = false;

        GetComponent<Camera>().orthographic = false;

        defaultRotation = transform.rotation;
        transform.position = position;
        transform.LookAt(lookAtPos);
    }

    //function to swoop back out to offset with default roation (temporary, not smoothed)
    public void SwoopOut()
    {
        if (!freeCam)
            freeCam = true;

        GetComponent<Camera>().orthographic = true;

        transform.position = offset;
        transform.rotation = defaultRotation;
    }
}
