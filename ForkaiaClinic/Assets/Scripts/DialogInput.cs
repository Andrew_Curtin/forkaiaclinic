﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogInput : MonoBehaviour
{
    public List<DialogTree> dialogTrees;
    public TextAsset inputFile;
    bool inputtingMessage;
    bool inputtingAnswer;
    string tempString = "";
    List<string> messageInput;
    List<string> answerInput;

    // Start is called before the first frame update
    void Start()
    {
        dialogTrees = new List<DialogTree>();
        messageInput = new List<string>();
        answerInput = new List<string>();
        char[] temp = inputFile.text.ToCharArray();

        for (int i = 0; i < temp.Length; i++)
        {
            if(temp[i] == '-' && (i+2<temp.Length && temp[i+2] == '\n')) //changed +1 to +2
            {
                inputtingMessage = true;
                i += 2;
                tempString = "";
                continue;
            }

            if (inputtingMessage)
            {
                //if we find the start of the answers, begin answer input
                if(temp[i] == '(' && temp[i+1] == 'A' && temp[i+2] == ')')
                {
                    inputtingMessage = false;
                    messageInput.Add(tempString);
                    tempString = "";
                    inputtingAnswer = true;
                    i--;
                    continue;
                }

                //ignoring new lines for now, just adding spaces
                if(temp[i] == '\n')
                {
                    tempString += ' ';
                    continue;
                }

                tempString += temp[i];

                //if we hit our character limit
                if(tempString.Length >= 150)
                {
                    for(int j = 0; j < tempString.Length; j++)
                    {
                        //go back to first available space ' ' char
                        if (tempString[tempString.Length-1-j] == ' ')
                        {
                            //remove from ' ' down to end
                            tempString = tempString.Remove(tempString.Length - 1 - j);

                            //set index to where the space is
                            i -= j;
                            
                            //add tempString to messages
                            messageInput.Add(tempString);
                            tempString = "";
                            break;
                        }
                        //continue at our new index, which will be right after space
                        //continue;
                    }//end for
                }//end if
            }//end message input

            if (inputtingAnswer)
            {
                if(temp[i] == '\n')
                {
                    //add tempString to answer array, clear tempString
                    answerInput.Add(tempString);
                    tempString = "";

                    int rightAnswerIndexTemp = 0;

                    //check if next 9 chars == "(Answer: "
                    for(int j = 0; j < 9; j++)
                    {
                        tempString += temp[i+1 + j];
                    }

                    if (tempString == "(Answer: ")
                    {
                        switch (temp[i+10])
                        {
                            case 'A':
                                rightAnswerIndexTemp = 0;
                                break;
                            case 'B':
                                rightAnswerIndexTemp = 1;
                                break;
                            case 'C':
                                rightAnswerIndexTemp = 2;
                                break;
                            case 'D':
                                rightAnswerIndexTemp = 3;
                                break;
                            case 'E':
                                rightAnswerIndexTemp = 4;
                                break;
                            default:
                                rightAnswerIndexTemp = 0;
                                break;
                        }
                        dialogTrees.Add(new DialogTree(messageInput.ToArray(), answerInput.ToArray(), rightAnswerIndexTemp));
                        inputtingMessage = false;
                        messageInput.Clear();
                        answerInput.Clear();
                        inputtingAnswer = false;
                        tempString = "";
                    }//end if "(Answer: "
                    else
                    {
                        tempString = "";
                    }
                    continue;
                }// end if '\n'

                tempString += temp[i];

            }//end answer input
        }//end for loop

        //Test log
        /*for (int k = 0; k < dialogTrees.Count; k++)
        {
            Debug.Log("DialogTree " + (k + 1));

            for (int l = 0; l < dialogTrees[k].messages.Length; l++)
            {
                Debug.Log("Message " + (l + 1) + ": " + dialogTrees[k].messages[l]);
            }

            for (int l = 0; l < dialogTrees[k].answers.Length; l++)
            {
                Debug.Log(dialogTrees[k].answers[l]);
            }

            Debug.Log("Correct Answer: " + dialogTrees[k].answers[dialogTrees[k].rightAnswerIndex]);
        }*/

    }//end start
}//end class
