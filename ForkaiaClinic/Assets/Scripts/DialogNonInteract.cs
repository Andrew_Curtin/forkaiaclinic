﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogNonInteract : MonoBehaviour
{
    public TextAsset inputFile;
    public List<string> messages;
    public Vector3 dialogPos;
    public Vector3 lookAtPos;
    public bool dialogActive = false;
    GameObject indicator;
    GameManagerController GameManager;

    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameManagerController.getInstance();
        messages = new List<string>();
        if(inputFile != null)
        {
            GetInputFromFile();
        }
        else
        {
            messages.Add("This is a default message.");
        }
        indicator = GameObject.Find("Indicator");
    }

    void GetInputFromFile()
    {
        char[] temp = inputFile.text.ToCharArray();
        string tempString = "";

        for (int i = 0; i < temp.Length; i++)
        {

            if (temp[i] == '\n')
            {
                tempString += ' ';
                continue;
            }

            tempString += temp[i];

            //if we hit our character limit
            if (tempString.Length >= 150)
            {
                for (int j = 0; j < tempString.Length; j++)
                {
                    //go back to first available space ' ' char
                    if (tempString[tempString.Length - 1 - j] == ' ')
                    {
                        //remove from ' ' down to end
                        tempString = tempString.Remove(tempString.Length - 1 - j);

                        //set index to where the space is
                        i -= j;

                        //add tempString to messages
                        messages.Add(tempString);
                        tempString = "";
                        break;
                    }
                    //continue at our new index, which will be right after space
                    //continue;
                }//end for
            }//end if
        }
        messages.Add(tempString);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!dialogActive && other.CompareTag("Player"))
        {
            indicator.transform.position = new Vector3(transform.position.x, 3.5f, transform.position.z);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (!dialogActive && other.CompareTag("Player"))
        {
            indicator.transform.position = Vector3.down * 5 + Vector3.forward * 5;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.Space) && !dialogActive && other.CompareTag("Player")
            /*&& check if other is facing us enough*/)
        {
            dialogActive = true;

            GameManager.ActivateMessage(this);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(dialogPos+transform.position, 0.5f);
        Gizmos.DrawWireSphere(transform.TransformDirection(dialogPos) + transform.position, 0.5f);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(lookAtPos + transform.position, 0.5f);
    }
}
