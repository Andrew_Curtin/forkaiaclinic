﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTree/* : MonoBehaviour*/
{
    public string[] messages;
    public string[] answers;
    public int rightAnswerIndex;

    public DialogTree(string[] inputMessages, string[] inputAnswers, int inputRightAnswerIndex)
    {
        messages = inputMessages;
        answers = inputAnswers;
        rightAnswerIndex = inputRightAnswerIndex;
    }

    public DialogTree()
    {
        messages = new string[2];
        messages[0] = "This is a default message. Click next to continue.";
        messages[1] = "The correct answer is (C)";
        answers = new string[5];
        answers[0] = "(A)";
        answers[1] = "(B)";
        answers[2] = "(C)";
        answers[3] = "(D)";
        answers[4] = "(E)";
        rightAnswerIndex = 2;
    }
}
