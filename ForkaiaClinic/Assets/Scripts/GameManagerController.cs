﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerController
{
    public static GameManagerController instance = null;

    CameraController camera;
    PlayerController player;
    DialogInteract currentDialog;
    DialogNonInteract currentMessage;
    UIController userInterface;

    List<QACategory> QAs;
    List<DialogTree> answeredQuestions;

    public static GameManagerController getInstance()
    {
        if (instance == null)
            instance = new GameManagerController();

        return instance;
    }

    public void SetPlayer(PlayerController player)
    {
        this.player = player;
        if (this.player != null)
            Debug.Log("player set");
        else
            Debug.Log("Error: player not set");
    }

    public void SetCamera(CameraController camera)
    {
        this.camera = camera;
        if (this.camera != null)
            Debug.Log("camera set");
        else
            Debug.Log("Error: camera not set");
    }

    public void SetUserInterface(UIController userInterface)
    {
        this.userInterface = userInterface;
        if (this.userInterface != null)
            Debug.Log("userInterface set");
        else
            Debug.Log("Error: userInterface not set");
    }

    public void ActivateDialog(DialogInteract dialog)
    {
        currentDialog = dialog;

        //stop player movement and fade player model
        player.FadeOut();

        //Camera /*Re-added*/
        //camera.SwoopIn(dialog.dialogPos + dialog.transform.position, dialog.lookAtPos+dialog.transform.position);
        camera.SwoopIn(dialog.transform.TransformDirection(dialog.dialogPos) + dialog.transform.position, dialog.lookAtPos + dialog.transform.position);

        //present dialog options, taken from currentDialog and given to userInterface
        userInterface.SetDialog(currentDialog.dialogTree);

        EnsureQAList();
    }

    public void DeactivateDialog()
    {
        currentDialog.dialogActive = false;
        currentDialog = null;

        //clean up ui
        userInterface.SetDefaultUI();

        //fade player back in and give movement back to player
        player.FadeIn();

        //Camera /*Re-added*/
        camera.SwoopOut();
    }

    public void ActivateMessage(DialogNonInteract dialog)
    {
        currentMessage = dialog;
        //camera.SwoopIn(dialog.dialogPos + dialog.transform.position, dialog.lookAtPos+dialog.transform.position);
        camera.SwoopIn(dialog.transform.TransformDirection(dialog.dialogPos) + dialog.transform.position, dialog.lookAtPos + dialog.transform.position);
        player.FadeOut();
        userInterface.SetMessageDialog(dialog);
    }

    public void DeactivateMessage()
    {
        if (currentMessage != null)
            currentMessage.dialogActive = false;

        if (currentDialog != null)
            currentDialog.dialogActive = false;
        currentDialog = null;

        currentMessage = null;
        camera.SwoopOut();
        player.FadeIn();
        userInterface.SetDefaultUI();
    }

    public void CorrectAnswer()
    {
        if (currentDialog != null && currentDialog.dialogInput != null)
        {
            for (int i = 0; i < QAs.Count; i++)
            {
                if (QAs[i].name == currentDialog.dialogInput.inputFile.name)
                {
                    QAs[i].Correct();
                    return;
                }
            }
        }
        else
        {
            QAs[QAs.Count - 1].Correct();
        }
    }
    public void IncorrectAnswer()
    {
        if (currentDialog != null && currentDialog.dialogInput != null)
        {
            for (int i = 0; i < QAs.Count; i++)
            {
                if (QAs[i].name == currentDialog.dialogInput.inputFile.name)
                {
                    QAs[i].Incorrect();
                    return;
                }
            }
        }
        else
        {
            QAs[QAs.Count - 1].Incorrect();
        }
    }

    class QACategory
    {
        public string name { get; set; }
        public int numberCorrect { get; set; }
        public int numberAsked { get; set; }
        //public int numberAvailable;

        public QACategory(string name, int numberAsked, int numberCorrect)
        {
            this.name = name;
            this.numberAsked = numberAsked;
            this.numberCorrect = numberCorrect;
        }

        public void Correct()
        {
            numberCorrect++;
            numberAsked++;
        }
        public void Incorrect()
        {
            numberAsked++;
        }
    }

    public void ScoreTestingDebugLog()
    {
        EnsureQAList();

        for (int i = 0; i < QAs.Count; i++)
        {
            Debug.Log(QAs[i].name + ": " + QAs[i].numberCorrect + "/" + QAs[i].numberAsked);
        }
    }

    public void DrawScoreDisplay()
    {
        EnsureQAList();
        GameObject scoreCanvas = GameObject.Find("ScoreCanvas");

        if (scoreCanvas.GetComponent<Canvas>().enabled)
        {
            scoreCanvas.GetComponent<Canvas>().enabled = false;

            foreach (GameObject temp in GameObject.FindGameObjectsWithTag("Respawn"))
                if (temp.name != "ExampleCategoryText" && temp.name != "ExampleScoreText")
                    GameObject.Destroy(temp.gameObject);

            return;
        }

        scoreCanvas.GetComponent<Canvas>().enabled = true;
        GameObject exampleCategoryText = GameObject.Find("ExampleCategoryText");
        GameObject exampleScoreText = GameObject.Find("ExampleScoreText");
        GameObject categoryText;
        GameObject ratioText;


        /*Parent of RectTransform is being set with parent property. Consider using the SetParent method instead,
         *  with the worldPositionStays argument set to false. This will retain local orientation and scale rather
         *   than world orientation and scale, which can prevent common UI scaling issues.*/

        for (int i = 0; i < QAs.Count; i++)
        {
            categoryText = GameObject.Instantiate(exampleCategoryText, scoreCanvas.transform, false);
            ratioText = GameObject.Instantiate(exampleScoreText, scoreCanvas.transform, false);

            categoryText.GetComponent<Text>().text = QAs[i].name;
            ratioText.GetComponent<Text>().text = QAs[i].numberCorrect + "   /   " + QAs[i].numberAsked;

            //categoryText.GetComponent<RectTransform>().sizeDelta = new Vector2(-50, -50 + 50 * i);
            //ratioText.GetComponent<RectTransform>().sizeDelta = new Vector2(200, -50 + 50 * i);
            categoryText.transform.localPosition = new Vector3(-50f, 50 - 50 * i, 0);
            ratioText.transform.localPosition = new Vector3(200f, 50 - 50 * i, 0);
        }
        //exampleCategoryText.SetActive(false);
        //exampleScoreText.SetActive(false);
        exampleCategoryText.GetComponent<Text>().text = "";
        exampleScoreText.GetComponent<Text>().text = "";
    }

    void EnsureQAList()
    {
        if (QAs == null)
        {
            QAs = new List<QACategory>();
            QAs.Add(new QACategory("Anatomy and Physiology", 0, 0));
            QAs.Add(new QACategory("Applied Mathematics", 0, 0));
            QAs.Add(new QACategory("TestingFormatting3", 0, 0));

            //last will alwasy be misc
            QAs.Add(new QACategory("Misc", 0, 0));
        }
    }

    public void AddToAnswered(DialogTree tree)
    {
        if (answeredQuestions == null)
            answeredQuestions = new List<DialogTree>();

        answeredQuestions.Add(tree);
    }

    public bool CheckIfAnswered(DialogTree checkTree)
    {
        if (answeredQuestions == null)
            answeredQuestions = new List<DialogTree>();

        foreach (DialogTree tree in answeredQuestions)
            if (tree == checkTree)
                return true;

        return false;
    }

    public void DefaultResponse(string response, DialogInteract dialog)
    {
        List<string> my_messages = new List<string>();

        if (response == null || response == "")
        {
            my_messages.Add("I have no more questions for you.\n\n*This is a default response*");
            my_messages.Add("Yay!");
            my_messages.Add("*Default Yay*");
        }
        else
        {
            //should break up into 150 character messages
            my_messages.Add(response);
        }

        //turn off QA format and display just the message box with our response
        //could check to see if other questions are unanswered and direct player to those rooms
        //


        //camera swoop
        currentDialog = dialog;
        camera.SwoopIn(dialog.transform.TransformDirection(dialog.dialogPos) + dialog.transform.position, dialog.lookAtPos + dialog.transform.position);
        player.FadeOut();

        
        userInterface.SetDefaultMessage(my_messages);
    }
}
