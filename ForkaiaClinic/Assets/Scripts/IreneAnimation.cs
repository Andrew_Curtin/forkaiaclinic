﻿//JasperNatag-oy
//9/15/20

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IreneAnimation : MonoBehaviour
{
    Animator animator;
    int isWalkingHash;
    int isWalkingdownHash;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        isWalkingHash = Animator.StringToHash("isWalking");
        isWalkingdownHash = Animator.StringToHash("isWalkingdown");
    }

    // Update is called once per frame
    void Update()
    {
        bool isWalking = animator.GetBool(isWalkingHash);
        bool isWalkingdown = animator.GetBool(isWalkingdownHash);
        bool wpress = Input.GetKey("w");
        bool spress = Input.GetKey("s");

        if (!isWalking && wpress)
        {
            animator.SetBool(isWalkingHash, true);
        }

        if (!isWalkingdown && spress)
        {
            animator.SetBool(isWalkingdownHash, true);
        }

        if (isWalking && !wpress)
        {
            animator.SetBool(isWalkingHash, false);
        }

        if (isWalkingdown && !spress)
        {
            animator.SetBool(isWalkingdownHash, false);
        }

    }
}
