﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //calls when PLAY buttons is pressed
    public void PlayGame() 
    {
        //switchs to the the next scene in queue
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame() 
    {
        // Exits game
        Application.Quit();
    }
}

