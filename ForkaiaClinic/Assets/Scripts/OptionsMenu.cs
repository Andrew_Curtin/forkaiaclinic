﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.Events;

public class OptionsMenu : MonoBehaviour
{

    public AudioMixer audioMixer;
    public Slider volSlider;


    void Start() 
    {
        volSlider.value = PlayerPrefs.GetFloat("MVolume", 1f);
        audioMixer.SetFloat("volume", PlayerPrefs.GetFloat("MVolume"));
    }
    public void SetVolume(float volume) 
    {
        PlayerPrefs.SetFloat("MVolume", volume);
        audioMixer.SetFloat("volume",PlayerPrefs.GetFloat("MVolume"));
    }
}
