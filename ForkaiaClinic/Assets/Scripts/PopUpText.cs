﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpText : MonoBehaviour
{
    bool textFade;
    float textFadeTime;
    Text my_text; //0,0,0, 255
    Outline my_outline;//255, 255, 255, 128
    public float fadeRate;

    // Start is called before the first frame update
    void Start()
    {
        my_text = GetComponent<Text>();
        my_outline = GetComponent<Outline>();
        textFade = true;
        textFadeTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (textFade && Time.time > textFadeTime + 3f)
        {
            my_text.color = new Color(0, 0, 0, my_text.color.a - fadeRate / 500);
            my_outline.effectColor = new Color(255, 255, 255, my_outline.effectColor.a - fadeRate / 1000);
            if (my_text.color.a <= 0.03f)
            {
                ClearText();
            }
        }
    }

    public void SetText(string text)
    {
        textFade = true;
        textFadeTime = Time.time;
        my_text.color = Color.black;
        my_outline.effectColor = Color.white;
        my_text.text = text;
    }

    public void ClearText()
    {
        textFade = false;
        my_text.color = new Color(0, 0, 0, 0);
        my_outline.effectColor = new Color(255, 255, 255, 0);
        my_text.text = "";
    }
}
